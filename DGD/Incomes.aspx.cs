﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGD.Managers;
using DGD.Models;

namespace DGD
{
    public partial class Incomes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlDataBind(ddlMonths, getMonths());
                ddlDataBind(ddlYears, getYears());
                setDate(DateTime.Now);
            }
        }

        private void repReport1DataBind(Report1[] arr)
        {
            
            repReport1.DataSource = arr;
            repReport1.DataBind();
            if (arr == null)
            {
                lblMessage.ForeColor = Color.Red;
                lblMessage.Text = "На данный момент нет данных для показа<br />";
            }
            else { lblMessage.Text = ""; }

        }

        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            var arr = txtHidden.Text.Split('-');
            int reportType = Convert.ToInt32(arr[0]);
            int periodType = Convert.ToInt32(arr[1]);
            DateTime reportDate = new DateTime(Convert.ToInt32(ddlYears.SelectedValue),Convert.ToInt32(ddlMonths.SelectedValue),Convert.ToInt32(ddlDays.SelectedValue));

            if (reportType == 1)
            {
                switch (periodType)
                {
                    case 1: repReport1DataBind(ReportManager.GetSumReports(ReportManager.GetReport1ByDay(reportDate))); break;
                    case 2: repReport1DataBind(ReportManager.GetSumReports(ReportManager.GetReport1ByMonth(reportDate))); break;
                    case 3: repReport1DataBind(ReportManager.GetSumReports(ReportManager.GetReport1ByYear(reportDate))); break;
                }
            }
            else if (reportType == 2)
            {
                repReport1DataBind(null);
                
            }
        }

        private Dictionary<int, string> getDays(int year, int month)
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            int dayCount = DateTime.DaysInMonth(year, month);
            for (int i = 1; i <= dayCount; i++)
                dict.Add(i, i.ToString());
            return dict;
        }

        private Dictionary<int, string> getYears()
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            int year = DateTime.Now.Year;
            for (int i = year-5; i <= year; i++)
                dict.Add(i, i.ToString());

            return dict;
        }

        private Dictionary<int, string> getMonths()
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "Январь");
            dict.Add(2, "Февраль");
            dict.Add(3, "Март");
            dict.Add(4, "Апрель");
            dict.Add(5, "Май");
            dict.Add(6, "Июнь");
            dict.Add(7, "Июль");
            dict.Add(8, "Август");
            dict.Add(9, "Сентябрь");
            dict.Add(10, "Октябрь");
            dict.Add(11, "Ноябрь");
            dict.Add(12, "Декабрь");
            return dict;
        }

        private void ddlDataBind(DropDownList ddl, Dictionary<int, string> dict)
        {
            ddl.Items.Clear();
            ddl.DataSource = dict;
            ddl.DataTextField = "Value";
            ddl.DataValueField = "Key";
            ddl.DataBind();
            ddl.Items.Add(new ListItem() {Text="---", Value="0" });
        }

        private void setDate(DateTime date)
        {
            ddlYears.SelectedValue = date.Year.ToString();
            ddlMonths.SelectedValue = date.Month.ToString();
            ddlDataBind(ddlDays, getDays(date.Year,date.Month));
            ddlDays.SelectedValue = date.Day.ToString();
        }
    }


}