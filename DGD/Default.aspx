﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DGD.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <img src="Styles/images/office.jpg" style="width: 100%;" />
    </div>
    <div class="container-fluid">
        <div class="field-item even" property="content:encoded">
            <p class="rteright" style="margin-left: 288pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Приложение 10</span></span></p>
            <p class="rteright" style="margin-left: 288pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">к приказу Председателя</span></span></p>
            <p class="rteright" style="margin-left: 288pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Комитета государственных доходов</span></span></p>
            <p class="rteright" style="margin-left: 288pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Министерства финансов</span></span></p>
            <p class="rteright" style="margin-left: 288pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Республики Казахстан</span></span></p>
            <p class="rteright" style="margin-left: 288pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">от «<u>17</u>» октября 2014 года № 5</span></span></p>
            <p class="rtejustify" style="margin-left: 46pt;">&nbsp;</p>
            <p class="rtejustify" style="margin-left: 46pt;">&nbsp;</p>
            <p class="rtecenter" style="margin-left: 46pt;"><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Положение</span></span></strong></p>
            <p class="rtecenter"><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; о Департаменте государственных доходов по Мангистауской области</span></span></strong></p>
            <p class="rtecenter"><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Комитета государственных доходов Министерства</span></span></strong></p>
            <p class="rtecenter"><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">финансов Республики Казахстан</span></span></strong></p>
            <p class="rtejustify">&nbsp;</p>
            <p class="rtejustify">&nbsp;</p>
            <p class="rtecenter"><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">1. Общие положения</span></span></strong></p>
            <p class="rtejustify">&nbsp;</p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Департамент государственных доходов по Мангистауской области Комитета государственных доходов Министерства финансов Республики Казахстан (далее - Департамент) является территориальным органом Комитета государственных доходов Министерства финансов Республики Казахстан (далее- Комитет), уполномоченным на выполнение функций государственного управления и контроля в сфере таможенного дела, по обеспечению полноты и своевременности поступлений налогов, таможенных и других обязательных платежей в бюджет, исчисления, удержания, перечисления обязательных пенсионных взносов и обязательных профессиональных пенсионных взносов, исчисления и уплаты социальных отчислений, государственного регулирования производства, оборота этилового спирта и алкогольной продукции, табачных изделий, оборота отдельных видов нефтепродуктов и биотоплива, государственного регулирования и контроля в области реабилитации и банкротства (за исключением банков, страховых (перестраховочных) организаций и накопительных пенсионных фондов), участие в реализации налоговой политики и политики в сфере таможенного дела, участие в разработке и реализации таможенного регулирования в Республике Казахстан отношений, связанных с перемещением товаров через таможенную границу Таможенного союза, их перевозкой по единой таможенной территории Таможенного союза под таможенным контролем, временным хранением, таможенным декларированием, выпуском и использованием в соответствии с таможенными процедурами, проведении таможенного контроля, властных отношений между органами государственных доходов и лицами, реализующими права владения, пользования и распоряжения указанными товарами, а также по предупреждению, выявлению, пресечению, раскрытию и расследованию экономических и финансовых преступлений и правонарушений в пределах, предусмотренных законодательством, и иных функций в соответствии с законодательством Республики Казахстан.</span></span></p>
            <ol>
                <li class="rtejustify" value="2">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Департамент осуществляет свою деятельность в соответствии с Конституцией и законами Республики Казахстан, актами Президента и Правительства Республики Казахстан, иными нормативными правовыми актами, а также настоящим Положением.</span></span></li>
                <li class="rtejustify" value="3">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Департамент является юридическим лицом в организационно-правовой форме государственного учреждения, имеет печати и штампы со своим наименованием на казахском языке, бланки установленного образца, а также в соответствии с законодательством Республики Казахстан счета в органах казначейства Министерства финансов Республики Казахстан.</span></span></li>
                <li class="rtejustify" value="4">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Департамент вступает в гражданско-правовые отношения or собственного имени.</span></span></li>
                <li class="rtejustify" value="5">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Департамент выступает стороной гражданско-правовых отношений от имени государства, если оно уполномочено на это в соответствии с законодательством Республики Казахстан.</span></span></li>
                <li class="rtejustify" value="6">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Департамент по вопросам своей компетенции в установленном законодательством Республики Казахстан порядке принимает решения, оформляемые приказами Руководителя Департамента.</span></span></li>
                <li class="rtejustify" value="7">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Структура и лимит штатной численности утверждается в соответствии с законодательством Республики Казахстан.</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 9.15pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8. Юридический&nbsp;&nbsp; адрес&nbsp;&nbsp; Департамента:&nbsp;&nbsp; почтовый&nbsp;&nbsp; индекс&nbsp;&nbsp;&nbsp; 130000, Республика Казахстан, Мангистауская область, город Актау, микрорайон 5, 31А.</span></span></p>
            <p class="rtejustify">
                <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 9.Полное наименование государственного органа - республиканское<br>
                    государственное учреждение&nbsp; «Департамент государственных доходов по<br>
                    <sub>:</sub>Мангистауской области Комитета государственных доходов Министерства финансов Республики Казахстан».</span></span>
            </p>
            <ol>
                <li class="rtejustify" value="10">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Настоящее Положение является учредительным документом Департамента.</span></span></li>
                <li class="rtejustify" value="11">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Финансирование деятельности Департамента осуществляется из республиканского бюджета.</span></span></li>
                <li class="rtejustify" value="12">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Департаменту запрещается вступать в договорные отношения с субъектами предпринимательства на предмет выполнения обязанностей, являющихся функциями Департамента. Если Департаменту законодательными актами предоставлено право осуществлять приносящую доходы деятельность, то доходы, полученные от такой деятельности, направляются в доход республиканского бюджета.</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 91.75pt;">&nbsp;</p>
            <p class="rtecenter" style="margin-left: 91.75pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;"><strong>2. Задачи, функции, права и обязанности Департамента</strong></span></span></p>
            <p class="rtejustify" style="margin-left: 47.2pt;">&nbsp;</p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;"><strong><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Задачи </em></strong><strong><em>Департамента:</em></strong></span></span></p>
            <ol>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение в пределах полномочий экономической безопасности государства, законных прав и интересов субъектов предпринимательской деятельности, общества и государства;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">реализация стратегий и программ, обеспечивающих формирование государственной политики по выявлению и расследованию преступлений в сфере экономической деятельности, а также противодействие «теневой» экономике;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение полноты и своевременности поступления налогов, таможенных и других обязательных платежей в бюджет, а также специальных антидемпинговых и компенсационных пошлин;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение в пределах своей компетенции мер по защите национальной безопасности государств - членов Таможенного союза, жизни и здоровья человека, животного и растительного мира, окружающей среды, а также в соответствии с международным договором государств - членов Таможенного союза - мер по противодействию легализации (отмыванию) доходов, полученных преступным путем, и финансированию терроризма при осуществлении контроля за перемещением через таможенную границу Таможенного союза валюты государств - членов Таможенного союза, ценных бумаг и (или) валютных ценностей, дорожных чеков;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">в пределах своей компетенции обеспечение соблюдения исполнения международных актов, таможенного законодательства Таможенного союза, налогового, таможенного и иного законодательства Республики Казахстан;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение в пределах своей компетенции соблюдения мер таможенно-тарифного регулирования, запретов и ограничений в отношении товаров, перемещаемых через таможенную границу Таможенного союза;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение соблюдения прав и законных интересов лиц в области таможенного регулирования и создание условий для ускорения товарооборота через таможенную границу Таможенного союза;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение защиты прав интеллектуальной собственности при перемещении товаров через таможенную границу Таможенного союза;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">совершение таможенных операций и проведение таможенного контроля в рамках оказания взаимной административной помощи;</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 39.25pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">10) осуществление таможенного контроля после выпуска товаров;</span></span></p>
            <ol>
                <li class="rtejustify" value="11">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление государственного контроля в области применения трансфертных цен;</span></span></li>
                <li class="rtejustify" value="12">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление государственного регулирования производства и оборота этилового спирта и алкогольной продукции, табачных изделий, а также оборота отдельных видов нефтепродуктов и биотоплива;</span></span></li>
                <li class="rtejustify" value="13">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение полноты и своевременности исчисления, удержания и перечисления обязательных пенсионных взносов и обязательных профессиональных пенсионных взносов в единый накопительный пенсионный фонд, исчисления и уплаты социальных отчислений в Государственный фонд социального страхования;</span></span></li>
                <li class="rtejustify" value="14">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">предупреждение, выявление, пресечение, раскрытие и расследование экономических и финансовых преступлений и правонарушений в соответствии с компетенцией, установленной законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="15">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">государственный контроль за проведением реабилитационной процедуры и процедуры банкротства (за исключением банков, страховых (перестраховочных) организаций и единого накопительного пенсионного фонда);</span></span></li>
                <li class="rtejustify" value="16">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">выполнение иных задач, предусмотренных законодательством Республики Казахстан.</span></span></li>
            </ol>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;"><strong><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Функции Департамента:</em></strong></span></span></p>
            <ol>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">контроль за соблюдением законодательства, предусматривающего полноту, своевременность поступлений налогов, таможенных и других обязательных платежей в бюджет, а также специальных антидемпинговых и компенсационных пошлин;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">применение положений международных договоров в порядке, установленном Кодексом Республики Казахстан «О налогах и других обязательных платежах в бюджет» (Налоговый кодекс), Кодексом Республики Казахстан «О таможенном деле в Республике Казахстан» и соответствующими международными договорами;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">взаимодействие в пределах компетенции с другими государственными органами по обеспечению экономической безопасности Республики Казахстан;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение выполнения международных обязательств Республики Казахстан в пределах компетенции Департамента;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">взаимодействие с центральными и государственными органами по осуществлению контроля за соблюдением таможенного законодательства Таможенного союза, налогового, таможенного и иного законодательства Республики Казахстан;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля и надзора за деятельностью физических и юридических лиц в пределах компетенции Департамента;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление налогового и таможенного администрирования;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление налогового контроля в соответствии с налоговым законодательством Республики Казахстан и таможенного контроля (в том числе после выпуска товаров) в соответствии с таможенным законодательством Таможенного союза и Республики Казахстан;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">участие в модернизации и ре-инжиниринге бизнес-процессов налогового и таможенного администрирования;</span></span></li>
            </ol>
            <p class="rtejustify">&nbsp;</p>
            <ol>
                <li class="rtejustify" value="10">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">оказание электронных услуг с применением информационных систем в соответствии с законодательством Республики Казахстан об информатизации;</span></span></li>
                <li class="rtejustify" value="11">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">оказание государственных услуг в соответствии со стандартами и регламентами оказания государственных услуг;</span></span></li>
                <li class="rtejustify" value="12">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">эксплуатация информационных систем, систем связи и систем передачи данных, технических средств таможенного контроля, а также средств защиты в соответствии с законодательством Республики Казахстан;</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 39.95pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">13)&nbsp; использование системы управления рисками;</span></span></p>
            <ol>
                <li class="rtejustify" value="14">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление налоговых проверок в порядке, установленном налоговым законодательством Республики Казахстан, таможенных проверок в порядке, предусмотренном таможенным законодательством Таможенного союза и Республики Казахстан, проверок по вопросам трансфертного ценообразования в порядке, предусмотренном законодательством Республики Казахстан о трансфертном ценообразовании;</span></span></li>
                <li class="rtejustify" value="15">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление внеплановых проверок в порядке, предусмотренном законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="16">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление возврата (зачета) излишне (ошибочно) уплаченных или излишне взысканных сумм налогов, таможенных пошлин, таможенных сборов, других обязательных платежей в бюджет и иных денег в порядке, предусмотренном законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="17">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление взаимодействия с государственными органами и иными организациями посредством информационных систем в порядке, установленном законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="18">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">размещение на Интернет-ресурсе информации в соответствии с законодательством Республики Казахстан по вопросам, относящимся к компетенции органов государственных доходов;</span></span></li>
                <li class="rtejustify" value="19">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">рассмотрение жалоб налогоплательщиков (налогового агента, оператора), декларанта и иных лиц, осуществляющих деятельность в сфере таможенного дела, на уведомления о результатах налоговой, таможенной проверок, а также действия (бездействие) должностных &nbsp;лиц органов государственных доходов в порядке и сроки, установленные налоговым и таможенным законодательством Республики Казахстан;</span></span></li>
            </ol>
            <ol>
                <li class="rtejustify" value="20">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">организация и осуществление работы по принудительному взысканию налоговой задолженности, задолженности по таможенным платежам, налогам и пеням, задолженности по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам и социальным отчислениям;</span></span></li>
                <li class="rtejustify" value="21">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">составление протоколов и рассмотрение дел об административных правонарушениях, осуществление административного задержания, а также применение других мер, предусмотренных законодательством Республики Казахстан об административных правонарушениях;</span></span></li>
                <li class="rtejustify" value="22">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">пересмотр не вступивших в законную силу постановлений по делам об административных правонарушениях в порядке, предусмотренном законодательством Республики Казахстан об административных правонарушениях;</span></span></li>
                <li class="rtejustify" value="23">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">определение объектов налогообложения и (или) объектов, связанных с налогообложением, на основе косвенных методов (активов, обязательств, оборота, затрат, расходов) в случае нарушения порядка ведения учета;</span></span></li>
                <li class="rtejustify" value="24">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">рассмотрение вопросов по изменению сроков исполнения налогового обязательства по уплате налогов в соответствии с законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="25">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление разъяснений и предоставление комментариев по вопросам, связанным с возникновением, исполнением и прекращением налогового обязательства;</span></span></li>
                <li class="rtejustify" value="26">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">контроль и анализ налоговых и неналоговых поступлений в пределах компетенции, установленной нормативными правовыми актами (кроме поступлений доли прибыли государственных предприятий, дивидендов на пакеты акций, являющихся государственной собственностью, от арендной платы за пользование комплексом «Байконур», от аренды и продажи республиканского государственного имущества, от приватизации объектов государственной собственности, поступлений от операций с капиталом);</span></span></li>
                <li class="rtejustify" value="27">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля за соблюдением порядка учета, хранения, оценки, дальнейшего использования и реализации имущества, обращенного (подлежащего обращению) в собственность государства;</span></span></li>
                <li class="rtejustify" value="28">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">выдача лицензии на хранение, оптовую и розничную реализацию алкогольной продукции;</span></span></li>
                <li class="rtejustify" value="29">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">взаимодействие с местными государственными органами по осуществлению контроля над производством и оборотом этилового спирта, алкогольной продукции, табачных изделий, а также оборотом нефтепродуктов И биотоплива;</span></span></li>
                <li class="rtejustify" value="30">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля за соблюдением законодательства Республики Казахстан о лицензировании при осуществлении деятельности в сфере производства и оборота этилового спирта и алкогольной продукции, производства табачных изделий;</span></span></li>
                <li class="rtejustify" value="31">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">ведение контроля, учета и анализа балансов объемов производства и оборота табачных изделий;</span></span></li>
                <li class="rtejustify" value="32">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля за производством и оборотом этилового спирта и алкогольной продукции;</span></span></li>
                <li class="rtejustify" value="33">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля над оборотом нефтепродуктов и биотоплива;</span></span></li>
                <li class="rtejustify" value="34">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">контроль за соблюдением минимальных цен при реализации алкогольной продукции и табачных изделий;</span></span></li>
                <li class="rtejustify" value="35">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление сотрудничества с соответствующими органами иностранных государств, международными организациями по вопросам отнесенным к ведению органов государственных доходов;</span></span></li>
                <li class="rtejustify" value="36">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">выявление признаков ложного и преднамеренного банкротства;</span></span></li>
                <li class="rtejustify" value="37">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление государственного контроля за проведением реабилитационной процедуры и процедуры банкротства;</span></span></li>
            </ol>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">38) осуществление контроля за соблюдением порядка проведения электронного аукциона по продаже имущества (активов) должника;</span></span></p>
            <ol>
                <li class="rtejustify" value="39">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">назначение реабилитационным или банкротным управляющим кандидатуры, представленной собранием кредиторов;</span></span></li>
                <li class="rtejustify" value="40">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">внесение предложений по установлению особых условий и порядка реализации имущественной массы и дополнительных требований к покупателям объектов имущественной массы при банкротстве организаций и индивидуальных предпринимателей, являющихся субъектами естественной монополии или субъектами рынка, занимающих доминирующее или монопольное положение на соответствующем товарном рынке, либо имеющих важное стратегическое значение для экономики республики, способных оказать влияние на жизнь, здоровье граждан, национальную безопасность или окружающую среду, в том числе организаций, пакеты акций (доли участия) которых отнесены к стратегическим объектам в соответствии с законодательством Республики Казахстан, а также признанных банкротами по инициативе государства, для которых подобный порядок предусмотрен Законом Республики Казахстан «О реабилитации и банкротстве»;</span></span></li>
            </ol>
            <p class="rtejustify">
                <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">41) рассмотрение текущей информации реабилитационного<br>
                    управляющего о ходе осуществления реабилитационной процедуры,<br>
                    временного управляющего о ходе осуществления сбора сведений о финансовом<br>
                    состоянии должника и процедуры банкротства, банкротного управляющего о<br>
                    ходе проведения процедуры банкротства;</span></span>
            </p>
            <p class="rtejustify">
                <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">42) согласование признания отсутствующего должника банкротом и его<br>
                    Ликвидации без возбуждения процедуры банкротства с учетом заключения<br>
                    временного управляющего об отсутствии должника по адресу, указанному в<br>
                    выявлении &nbsp;о&nbsp; признании должника банкротом,&nbsp; и&nbsp; отсутствии&nbsp; имущества (активов), за счет которого возможно осуществить процедуру банкротства;</span></span>
            </p>
            <p class="rtejustify">
                <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">43) согласование продажи временным управляющим имущества банкрота<br>
                    в случае, предусмотренном Законом Республики Казахстан «О реабилитации и банкротстве»;</span></span>
            </p>
            <ol>
                <li class="rtejustify" value="44">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">рассмотрение жалоб на действия (бездействие) временного администратора, реабилитационного, временного и банкротного управляющих;</span></span></li>
                <li class="rtejustify" value="45">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление запроса и получение от государственных органов, юридических лиц и их должностных лиц информации о неплатежеспособных и</span></span></li>
            </ol>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">несостоятельных должниках;</span></span></p>
            <ol>
                <li class="rtejustify" value="46">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">представление временному и банкротному управляющим информации о наличии и номерах банковских счетов лица, по которому имеется вступившее в законную силу решение суда о признании банкротом, остатках и движении денег на этих счетах;</span></span></li>
                <li class="rtejustify" value="47">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">оспаривание в суде решений и действий (бездействия) временного администратора, реабилитационного, временного и банкротного управляющих в случае выявления нарушений Закона Республики Казахстан «О реабилитации и банкротстве»;</span></span></li>
                <li class="rtejustify" value="48">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">представление разъяснений и комментариев по введению, проведению и прекращению процедур реабилитации и банкротства в пределах своей компетенции;</span></span></li>
                <li class="rtejustify" value="49">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">направление уведомлений суду о снятии с регистрации временного Управляющего, временного администратора, а также собранию кредиторов о снятии с регистрации реабилитационного либо банкротного управляющих;</span></span></li>
            </ol>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 50)отстранение реабилитационного и банкротного управляющих;</span></span></p>
            <ol>
                <li class="rtejustify" value="51">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление таможенной очистки товаров, перемещаемых через таможенную границу Таможенного союза, в том числе с использованием информационных технологий;</span></span></li>
                <li class="rtejustify" value="52">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление таможенного декларирования наличных денежных средств и денежных инструментов;</span></span></li>
                <li class="rtejustify" value="53">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение, соблюдения порядка перемещения физическими лицами через таможенную границу Таможенного союза товаров и транспортных средств для личного пользования;</span></span></li>
                <li class="rtejustify" value="54">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">организация таможенного декларирования и таможенного контроля товаров для личного пользования, пересылаемых в международных почтовых отправлениях;</span></span></li>
                <li class="rtejustify" value="55">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">организация таможенного декларирования и таможенного контроля товаров, перемещаемых через таможенную границу Таможенного союза отдельными категориями иностранных лиц;</span></span></li>
                <li class="rtejustify" value="56">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление в пределах компетенции контроля за лицами, осуществляющими деятельность в сфере таможенного дела;</span></span></li>
                <li class="rtejustify" value="57">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">рассмотрение в пределах компетенции заявлений юридических лиц о включении в реестр на осуществление деятельности в сфере таможенного дела и принятие решения по результатам рассмотрения указанных заявлений;</span></span></li>
                <li class="rtejustify" value="58">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение на постоянной основе своевременного информирования и консультирования участников внешнеэкономической и иной деятельности в &lt;|фере таможенного дела по вопросам, касающимся . таможенного законодательства Республики Казахстан, и иным вопросам, входящим в компетенцию органов государственных доходов, в том числе об изменениях и дополнениях в таможенное законодательство Таможенного союза и Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="59">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление таможенного контроля за условно выпущенными товарами в соответствии с таможенным законодательством Таможенного союза и Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="60">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение в пределах компетенции соблюдения мер таможенно-тарифного регулирования, запретов и ограничений в отношении товаров и транспортных средств, перемещаемых через таможенную границу Таможенного союза;</span></span></li>
                <li class="rtejustify" value="61">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">в пределах компетенции осуществление во взаимодействии с органами национальной безопасности и другими соответствующими государственными органами мер по обеспечению защиты Государственной границы Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="62">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение в пределах компетенции охраны таможенной границы Таможенного союза и контроля за соблюдением режима зоны таможенного контроля;</span></span></li>
                <li class="rtejustify" value="63">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля за правильностью определения страны происхождения товаров;</span></span></li>
            </ol>
            <ol>
                <li class="rtejustify" value="64">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля за правильностью предоставления тарифных преференций;</span></span></li>
                <li class="rtejustify" value="65">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля за классификацией товаров в соответствии с Товарной номенклатурой внешнеэкономической деятельности Таможенного союза (далее - ТН ВЭД ТС);</span></span></li>
                <li class="rtejustify" value="66">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление контроля за правильностью определения таможенной стоимости товаров;</span></span></li>
                <li class="rtejustify" value="67">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление таможенного контроля за соблюдением требований и условий таможенных процедур, при помещении под которые товары не приобретают статус товаров Таможенного союза, а также требований и условий, предъявляемых к завершению указанных таможенных процедур;</span></span></li>
                <li class="rtejustify" value="68">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">принятие предварительных решений о классификации товаров в соответствии с ТН ВЭД ТС и иных решений в соответствии с таможенным законодательством Таможенного союза и Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="69">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">рассмотрение документов, определенных таможенным законодательством Таможенного союза и Республики Казахстан, на основании которых предоставляется освобождение от уплаты таможенных платежей и налогов;</span></span></li>
                <li class="rtejustify" value="70">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление сбора информации по ввозу в Республику Казахстан или вывозу из Республики Казахстан культурных ценностей, наличной валюты, документарных ценных бумаг на предъявителя, векселей, чеков, подлежащих финансовому мониторингу, за исключением ввоза или вывоза, осуществляемых С территории, которая является составной частью таможенной территории Таможенного союза, на территорию, которая является составной частью таможенной территории Таможенного союза, в соответствии с законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="71">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление сбора, обобщения и анализа статистической и оперативной информации о готовящихся и совершенных преступлениях и правонарушениях, входящих в компетенцию органов государственных доходов;</span></span></li>
                <li class="rtejustify" value="72">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление досудебного производства (упрощенного досудебного производства), предварительного следствия, дознания по делам об экономических и финансовых преступлениях и правонарушениях в порядке, предусмотренном уголовно-процессуальным законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="73">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление оперативно-розыскной деятельности в соответствии с законодательством Республики Казахстан об оперативно-розыскной деятельности;</span></span></li>
                <li class="rtejustify" value="74">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обеспечение соблюдения требований по защите информации ь. эксплуатации средств защиты информации в соответствии с законодательством Республики Казахстан;</span></span></li>
            </ol>
            <ol>
                <li class="rtejustify" value="75">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">в пределах компетенции осуществление розыска лиц по уголовным делами ответчиков при неизвестности их места пребывания по искам, предъявленным в интересах государства, по постановлению суда;</span></span></li>
                <li class="rtejustify" value="76">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">выработка и реализация мер по повышению эффективности деятельности органов государственных доходов в сфере борьбы с экономическими и финансовыми преступлениями и правонарушениями;</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 40.95pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">77) осуществление досудебного производства (упрощенного досудебного<sub>г</sub></span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">производства), дознания по делам о преступлениях, связанных с контрабандой, уклонением от уплаты таможенных платежей, налогов и сборов в порядке, предусмотренном уголовно-процессуальным законодательством Республики Казахстан;</span></span></p>
            <ol>
                <li class="rtejustify" value="77">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">осуществление иных функций, предусмотренных законодательством Республики &nbsp;Казахстан.</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 13.25pt;">&nbsp;</p>
            <p class="rtecenter"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><em>Права и обязанности Департамента:</em></strong></span></span></p>
            <div class="rtejustify" style="clear: both;">
                &nbsp;
            </div>
            <ol>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">запрашивать, получать в установленном законодательством порядке от государственных органов, их должностных лиц и передавать им необходимую информацию и материалы;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">давать разъяснения и комментарии по применению действующего законодательства по вопросам, входящим в компетенцию Департамента;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">привлекать специалистов соответствующих государственных органов, консультантов и независимых экспертов из числа физических и юридических лиц Республики Казахстан и других государств для проведения экспертиз, проверок консультаций;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">рассматривать дела об административных правонарушениях, составлять по ним протокола и налагать административные взыскания в порядке, предусмотренном законодательством Республики Казахстан об административных правонарушениях;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">обращаться в суд, предъявлять иски в целях защиты прав и интересов Департамента в соответствии с законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">выносить решение о приостановлении операций с деньгами и (или) иным имуществом, в случае обнаружения признаков подозрительной операции, отвечающей одному или нескольким критериям, установленным пунктом 4 статьи 4 Закона Республики Казахстан «О противодействии легализации (отмыванию) доходов, полученных преступным путем, и финансированию терроризма», на срок до трех календарных дней;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">определять совместно с правоохранительными и специальными государственными органами порядок взаимодействия по обмену и передаче сведении и информации, связанных с легализацией (отмыванием) доходов, полученных преступным путем, и финансированием терроризма;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">рассматривать обращения, заявления и жалобы физических и юридических лиц по вопросам, входящим в компетенцию Департамента;</span></span></li>
            </ol>
            <p class="rtejustify">
                <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">9) требовать от физических и юридических лиц в случаях, определенных<br>
                    законодательством представление необходимых документов, отчетностей по<br>
                    установленным формам;</span></span>
            </p>
            <ol>
                <li class="rtejustify" value="10">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">взаимодействовать с другими государственными органами, международными организациями, участниками внешнеэкономической и иной деятельности в порядке, определенном законодательными актами Республики Казахстан, а также, на основании совместных актов соответствующих государственных органов по согласованию с ними;</span></span></li>
                <li class="rtejustify" value="11">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">представлять по запросу соответствующего уполномоченного органа сведения из собственных информационных систем в порядке, определенном законодательством Республики Казахстан;</span></span></li>
                <li class="rtejustify" value="12">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">направлять в пределах компетенции запрос производителям табачных изделий о предоставлении необходимых сведений для осуществления государственного регулирования производства и оборота табачных изделий;</span></span></li>
                <li class="rtejustify" value="13">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;направлять мотивированный отказ собранию кредиторов в назначении кандидатуры реабилитационного управляющего либо сообщение о снятии</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 21.2pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">реабилитационного управляющего с регистрации;</span></span></p>
            <p class="rtejustify">
                <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">14)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; по имеющимся в производстве материалам и уголовным делам иметь<br>
                    доступ к документам, материалам, статистическим данным и иным сведениям,<br>
                    а также требовать их представления от руководителей и других должностных</span></span>
            </p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">организаций, физических лиц, снимать с них копии, получать объяснения;</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;15) по имеющимся в производстве уголовным делам подвергать приводу лиц, уклоняющихся от явки по вызову;</span></span></p>
            <p class="rtejustify" style="margin-left: 25.3pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">16)изымать или производить выемку документов, товаров, предметов или иного&nbsp;&nbsp;&nbsp;&nbsp; имущества&nbsp;&nbsp;&nbsp;&nbsp; в&nbsp;&nbsp;&nbsp;&nbsp; соответствии&nbsp;&nbsp;&nbsp;&nbsp; с&nbsp;&nbsp;&nbsp;&nbsp; уголовно-процессуальным</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">законодательством и законодательством об административных правонарушениях Республики Казахстан;</span></span></p>
            <p class="rtejustify" style="margin-left: 25.3pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">17) использовать соответствующие изоляторы временного содержания, следственные изоляторы в порядке, предусмотренном законодательством</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Республики Казахстан;</span></span></p>
            <p class="rtejustify">
                <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 18)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; составлять протоколы и рассматривать дела об административных<br>
                    правонарушениях,&nbsp;&nbsp; осуществлять&nbsp;&nbsp; административное&nbsp;&nbsp; задержание,&nbsp;&nbsp; а&nbsp; также применять другие меры, предусмотренные законодательством Республики Казахстан об административных правонарушениях;</span></span>
            </p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;19)требовать производства ревизий, налоговых и других проверок, аудита и оценки от уполномоченных органов и должностных лиц в случаях, предусмотренных законодательством Республики Казахстан;</span></span></p>
            <ol>
                <li class="rtejustify" value="20">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">давать обязательные для исполнения предписания, представления физическим и юридическим лицам об устранении причин и условий, способствовавших совершению преступлений и иных правонарушений;</span></span></li>
                <li class="rtejustify" value="21">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">использовать информационные системы, обеспечивающие решение изложенных на органы государственных доходов задач, организовывать исследования в ходе предварительного следствия, дознания, производства по делам об административных правонарушениях в порядке, установленном законодательством Республики Казахстан.</span></span></li>
            </ol>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">22) осуществлять иные права и обязанности в соответствии с законодательством Республики Казахстан.</span></span></p>
            <p class="rtejustify" style="margin-left: 121.6pt;">&nbsp;</p>
            <p class="rtejustify" style="margin-left: 121.6pt;">&nbsp;</p>
            <p class="rtecenter" style="margin-left: 121.6pt;"><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">3. Организация деятельности Департамента</span></span></strong></p>
            <p class="rtejustify">&nbsp;</p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">1. Руководство Департаментом осуществляется Руководителем, который несёт персональную ответственность за выполнение возложенных на Департамент задач и осуществление им своих функций.</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">2. Руководитель Департамента назначается на должность и освобождается от должности в соответствии с законодательством Республики Казахстан.</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">3. Руководитель Департамента имеет заместителей, которые назначаются на должности и освобождаются от должностей в соответствии с законодательством Республики Казахстан.</span></span></p>
            <p class="rtejustify" style="margin-left: 38.75pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">4. &nbsp;<strong><em>Руководитель Департамента осуществляет следующие полномочия:</em></strong></span></span></p>
            <ol>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">определяет обязанности и полномочия своих заместителей, руководителей, работников и сотрудников структурных подразделений Департамента, руководителей территориальных органов Департамента;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">утверждает штатное расписание Департамента в пределах лимита штатной численности Департамента;</span></span></li>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">в соответствии с законодательством Республики Казахстан назначает на должности и освобождает от должностей:</span></span></li>
            </ol>
            <p class="rtejustify" style="margin-left: 36.6pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">работников и сотрудников Департамента;</span></span></p>
            <p class="rtejustify" style="margin-left: 36.6pt;"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">заместителей руководителей территориальных органов Департамента;</span></span></p>
            <ol>
                <li class="rtejustify" value="4">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">принимает меры дисциплинарной ответственности в установленном законодательством Республики Казахстан порядке;</span></span></li>
                <li class="rtejustify" value="5">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">утверждает положения о структурных подразделениях Департамента;</span></span></li>
                <li class="rtejustify" value="6">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">в установленном законодательством Республики Казахстан порядке решает вопросы командирования, предоставления отпусков, оказания материальной помощи, подготовки (переподготовки), повышения квалификации, поощрения, выплаты надбавок и премирования заместителей руководителя Департамента, работников и сотрудников Департамента, руководителей и заместителей руководителей территориальных органов Департамента;</span></span></li>
                <li class="rtejustify" value="7">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">несет персональную ответственность по противодействию коррупции;</span></span></li>
                <li class="rtejustify" value="8">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">несет персональную ответственность за достоверность информации, предоставляемой в Комитет;</span></span></li>
                <li class="rtejustify" value="9">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">в пределах компетенции подписывает акты Департамента;</span></span></li>
            </ol>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">10)&nbsp;&nbsp; представляет Департамент во всех государственных органах и иных организациях;</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">11) осуществляет иные полномочия, предусмотренные законодательством Республики Казахстан. Исполнение полномочий Руководителя Департамента в период его отсутствия осуществляется лицом его замещающим в соответствии с законодательством Республики Казахстан.</span></span></p>
            <p class="rtejustify" style="">&nbsp;</p>
            <p class="rtecenter" style=""><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">4. Имущество Департамента</span></span></strong></p>
            <p class="rtejustify" style="">&nbsp;</p>
            <ol>
                <li class="rtejustify">
                    <span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">Департамент имеет на праве оперативного управления обособленное имущество в случаях, предусмотренных законодательством Республики Казахстан. Имущество Департамента формируется за счет имущества, переданного ему собственником, а также имущества (включая денежные доходы), приобретенного в результате собственной деятельности и иных источников, не запрещенных законодательством Республики Казахстан.</span></span></li>
            </ol>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Имущество, закрепленное за Департаментом, относится к республиканской собственности.</span></span></p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Департамент не вправе самостоятельно отчуждать или иным способом распоряжаться закрепленным за ним имуществом и имуществом, приобретенным за счет средств, выданных ему по плану финансирования, если иное не установлено законодательством Республики Казахстан.</span></span></p>
            <p class="rtejustify" style="">&nbsp;</p>
            <p class="rtejustify" style="">&nbsp;</p>
            <p class="rtecenter" style=""><strong><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">5. Реорганизация и ликвидация Департамента</span></span></strong></p>
            <p class="rtejustify" style="">&nbsp;</p>
            <p class="rtejustify"><span style="font-size: 14px;"><span style="font-family: times new roman,times,serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Реорганизация и упразднение Департамента осуществляется в соответствии с законодательством Республики Казахстан.</span></span></p>
        </div>
    </div>
</asp:Content>
