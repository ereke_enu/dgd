﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DGD.Models;
using mng = DGD.Managers.BaseManager;

namespace DGD.Managers
{
    public static class MaterialManager
    {
        public static Material[] GetMaterials()
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            cmd.CommandText = @"select m.*, d1.Name as SpecificationName, d2.Name as LegalStateName, d3.Name as FinancialSourceName from Materials m
inner join Dictionaries d1 on m.SpecificationId=d1.Id
inner join Dictionaries d2 on m.LegalStateId = d2.Id
inner join Dictionaries d3 on m.FinancialSourceId=d3.Id";
            conn.Open();
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Material> lst = new List<Material>();
            while (reader.Read())
            {
                Material mat = new Material();
                mat.Id = Convert.ToInt32(reader["Id"]);
                mat.Name = reader["Name"].ToString();
                mat.SpecificationId = Convert.ToInt32(reader["SpecificationId"]);
                mat.LegalStateId = Convert.ToInt32(reader["LegalStateId"]);
                mat.ReceiptDate = Convert.ToDateTime(reader["ReceiptDate"]);
                mat.CurrencyId = Convert.ToInt32(reader["CurrencyId"]);
                mat.FilePath = reader["FilePath"].ToString();
                mat.CreateDate = Convert.ToDateTime(reader["CreateDate"]);
                mat.UserId = Convert.ToInt32(reader["UserId"]);
                mat.FinancialSourceId = Convert.ToInt32(reader["FinancialSourceId"]);
                mat.FinancialSourceName = reader["FinancialSourceName"].ToString();
                mat.SpecificationName = reader["SpecificationName"].ToString();
                mat.LegalStateName = reader["LegalStateName"].ToString();
                lst.Add(mat);
            }
            conn.Close();
            return lst.ToArray();
        }

        public static bool CreateMaterial(Material material)
        {

            var conn = mng.CreateConnection();
            var cmd = conn.CreateCommand();
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@Name", DbType = DbType.String, Value = material.Name });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@SpecificationId", DbType = DbType.Int32, Value = material.SpecificationId });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@LegalStateId", DbType = DbType.Int32, Value = material.LegalStateId });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ReceiptDate", DbType = DbType.Date, Value = material.ReceiptDate });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@CurrencyId", DbType = DbType.Int32, Value = material.CurrencyId });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@FilePath", DbType = DbType.String, Value = material.FilePath });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@CreateDate", DbType = DbType.DateTime, Value = material.CreateDate });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@UserId", DbType = DbType.Int32, Value = material.UserId });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@FinancialSourceId", DbType = DbType.Int32, Value = material.FinancialSourceId });
            cmd.CommandText = "INSERT INTO Materials VALUES(@Name,@SpecificationId,@LegalStateId,@ReceiptDate,@CurrencyId,@FilePath,@CreateDate,@UserId,@FinancialSourceId)";
            conn.Open();
            var res = mng.ExecuteNonQuery(cmd);
            conn.Close();
            return res;

        }
    }
}