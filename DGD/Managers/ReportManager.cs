﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DGD.Models;

namespace DGD.Managers
{
    public static class ReportManager
    {
        #region

        private static Report1 getReportFromReaderRow(SqlDataReader reader)
        {
            Report1 rep = new Report1();
            rep.Id = Convert.ToInt32(reader["Id"]);
            rep.DGDId = Convert.ToInt32(reader["DGDId"]);
            rep.ReceiptDate = Convert.ToDateTime(reader["ReceiptDate"]);
            rep.CreateDate = Convert.ToDateTime(reader["CreateDate"]);
            rep.UserId = Convert.ToInt32(reader["UserId"]);
            rep.TotalValue = Convert.ToDecimal(reader["TotalValue"]);
            rep.LocalValue = Convert.ToDecimal(reader["LocalValue"]);
            rep.RegionValue = Convert.ToDecimal(reader["RegionValue"]);
            rep.RespublicValue = Convert.ToDecimal(reader["RespublicValue"]);
            rep.SpecValue = Convert.ToDecimal(reader["SpecValue"]);
            return rep;
        }

        #endregion

        public static Report1[] GetReport1ByDay(DateTime reportDate)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            cmd.Parameters.Add(new SqlParameter() { ParameterName="@ReceiptDate", DbType=DbType.DateTime, Value=reportDate});
            cmd.CommandText = "select * from Report1 where ReceiptDate=@ReceiptDate";
            conn.Open();
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Report1> lst = new List<Report1>();
            while (reader.Read())
            {
                Report1 rep = getReportFromReaderRow(reader);
                lst.Add(rep);
            }
            conn.Close();
            return lst.ToArray();
        }

        public static Report1[] GetReport1ByMonth(DateTime reportDate)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@month", DbType = DbType.Int32, Value = reportDate.Month });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@year", DbType = DbType.Int32, Value = reportDate.Year });

            cmd.CommandText = "SELECT * FROM Report1 WHERE DATEPART(month, ReceiptDate)=@month and DATEPART(YEAR, ReceiptDate)=@year";
            conn.Open();
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Report1> lst = new List<Report1>();
            while (reader.Read())
            {
                Report1 rep = getReportFromReaderRow(reader);
                lst.Add(rep);
            }
            conn.Close();
            return lst.ToArray();
        }

        public static Report1[] GetReport1ByYear(DateTime reportDate)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@year", DbType = DbType.Int32, Value = reportDate.Year });

            cmd.CommandText = "SELECT * FROM Report1 WHERE DATEPART(YEAR, ReceiptDate)=@year";
            conn.Open();
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<Report1> lst = new List<Report1>();
            while (reader.Read())
            {
                Report1 rep = getReportFromReaderRow(reader);
                lst.Add(rep);
            }
            conn.Close();
            return lst.ToArray();
        }

        public static Report1[] GetSumReports(Report1[] arr)
        {
            //if (arr == null) return null;
            var dgds = DictionaryManager.GetDictionaries((int)DictionaryTypes.DGD);
            List<Report1> lst = new List<Report1>();
            foreach (var d in dgds)
            {
                decimal total = 0, local = 0, region = 0, respublic = 0, spec = 0;
                foreach (var rep in arr)
                {
                    if (rep.DGDId == d.Id)
                    {
                        total += rep.TotalValue;
                        local += rep.LocalValue;
                        region += rep.RegionValue;
                        respublic += rep.RespublicValue;
                        spec += rep.SpecValue;
                    }
                }
                Report1 report1 = new Report1();

                report1.DGDId = d.Id;
                report1.TotalValue = total;
                report1.LocalValue =local;
                report1.RegionValue = region;
                report1.RespublicValue = respublic;
                report1.SpecValue = spec;
                report1.DGDName = d.Name;
                lst.Add(report1);
            }
            return lst.ToArray();
        }

        public static bool CreateReport1(Report1 report){
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            bool res=  false;
            cmd.Parameters.Add(new SqlParameter() { DbType = DbType.Int32, ParameterName = "@DGDId", Value = report.DGDId });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@ReceiptDate", DbType = DbType.Date, Value = report.ReceiptDate });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@CreateDate", DbType = DbType.DateTime, Value = DateTime.Now });
            
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@UserId", DbType = DbType.Int32, Value = report.UserId });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@TotalValue", DbType = DbType.Decimal, Value = report.TotalValue });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@LocalValue", DbType = DbType.Decimal, Value = report.LocalValue });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@RegionValue", DbType = DbType.Decimal, Value = report.RegionValue });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@RespublicValue", DbType = DbType.Decimal, Value =report.RespublicValue });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@SpecValue", DbType = DbType.Decimal, Value = report.SpecValue });
            cmd.CommandText = "INSERT INTO Report1 VALUES(@DGDId,@ReceiptDate,@CreateDate,@UserId,@TotalValue,@LocalValue,@RegionValue,@RespublicValue,@SpecValue)";
            conn.Open();
            try
            {
                cmd.ExecuteNonQuery();
                res = true;
            }
            catch
            {
                res = false;
            }
            finally
            {
                conn.Close();
            }
            return res;
        }
    }
}