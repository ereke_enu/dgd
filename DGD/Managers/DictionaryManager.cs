﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DGD.Models;
using mng = DGD.Managers.BaseManager;

namespace DGD.Managers
{
    public static class DictionaryManager
    {
        public static BaseDictionary[] GetDictionaries(int typeId)
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            cmd.CommandText = "select * from Dictionaries where TypeId=" + typeId + " order by Code";
            conn.Open();
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<BaseDictionary> lst = new List<BaseDictionary>();
            while (reader.Read())
            {
                var obj = new BaseDictionary();
                obj.Id = Convert.ToInt32(reader["Id"]);
                obj.Name = reader["Name"].ToString();
                obj.TypeId = Convert.ToInt32(reader["TypeId"]);
                obj.Code = Convert.ToInt32(reader["Code"]);
                
                lst.Add(obj);
            }
            conn.Close();
            return lst.ToArray();
        }

        public static BaseDictionary[] GetDictionariesWithTypeName(int typeId)
        {
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            cmd.CommandText = "select d1.*,d2.Name as TypeName from Dictionaries d1 inner join Dictionaries d2 on d2.Id=d1.TypeId where d1.TypeId="+typeId+" order by d1.Code";
            conn.Open();
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            List<BaseDictionary> lst = new List<BaseDictionary>();
            while (reader.Read())
            {
                var obj = new BaseDictionary();
                obj.Id = Convert.ToInt32(reader["Id"]);
                obj.Name = reader["Name"].ToString();
                obj.TypeId = Convert.ToInt32(reader["TypeId"]);
                obj.Code = Convert.ToInt32(reader["Code"]);
                obj.TypeName = reader["TypeName"].ToString();
                lst.Add(obj);
            }
            conn.Close();
            return lst.ToArray();
        }

        public static bool CreateDictionary(BaseDictionary dict)
        {
            var conn = mng.CreateConnection();
            var cmd = conn.CreateCommand();
            cmd.Parameters.Add(new SqlParameter() { ParameterName="@Name", DbType=DbType.String, Value=dict.Name });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@TypeId", DbType = DbType.Int32, Value = dict.TypeId });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@Code", DbType = DbType.Int32, Value = dict.Code });
            cmd.CommandText = "insert into Dictionaries Values(@Name, @TypeId, @Code)";
            conn.Open();
            var res = mng.ExecuteNonQuery(cmd);
            conn.Close();
            return res;
        }


    }
}