﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using DGD.Models;

namespace DGD.Managers
{
    public static class UserManager
    {
        public static User GetUser(string login, string password)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DGDConnectionString"].ConnectionString);
            var cmd = conn.CreateCommand();
            cmd.CommandText = "select * from Users where Login=@login and Password=@password";
            cmd.Parameters.Add(new SqlParameter() {ParameterName="@login", DbType=DbType.String, Value=login });
            cmd.Parameters.Add(new SqlParameter() { ParameterName = "@password", DbType = DbType.String, Value = password });
            conn.Open();
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            User user = null;
            while (reader.Read())
            {
                user = new User();
                user.Id = Convert.ToInt32(reader["Id"]);
                user.FIO = reader["FIO"].ToString();
                user.Login = reader["Login"].ToString();
                user.Password = reader["Password"].ToString();
                user.RoleId = Convert.ToInt32(reader["RoleId"]);
            }
            conn.Close();
            return user;
        }
    }
}