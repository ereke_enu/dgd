﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGD.Models
{
    public class BaseDictionary
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TypeId { get; set; }
        public int Code { get; set; }
        public string TypeName { get; set; }

    }
}