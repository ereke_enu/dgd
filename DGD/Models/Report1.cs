﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGD.Models
{
    public class Report1
    {
        public int Id { get; set; }
        public int DGDId { get; set; }
        public DateTime ReceiptDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int UserId { get; set; }
        public decimal TotalValue { get; set; }
        public decimal LocalValue { get; set; }
        public decimal RegionValue { get; set; }
        public decimal RespublicValue { get; set; }
        public decimal SpecValue { get; set; }

        #region
        public string DGDName { get; set; }
        #endregion

    }
}