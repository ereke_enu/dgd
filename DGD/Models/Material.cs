﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGD.Models
{
    public class Material
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SpecificationId { get; set; }
        public int LegalStateId { get; set; }
        public DateTime ReceiptDate { get; set; }
        public int CurrencyId { get; set; }
        public string FilePath { get; set; }
        public DateTime CreateDate { get; set; }
        public int UserId { get; set; }
        public int FinancialSourceId { get; set; }

        public string SpecificationName { get; set; }
        public string LegalStateName { get; set; }
        public string FinancialSourceName { get; set; }

    }
}