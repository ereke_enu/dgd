﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Incomes.aspx.cs" Inherits="DGD.Incomes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="Styles/tabs.css" type="text/css" />
    <script src="Scripts/Chart.js" type="text/javascript"></script>
    <script>

        var data = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(220,220,220,0.2)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: "My Second dataset",
                    fillColor: "rgba(151,187,205,0.2)",
                    strokeColor: "rgba(151,187,205,1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]
        };


        $(document).ready(function () {

            window.onload = function () {
                var ctx = document.getElementById("chart-area").getContext("2d");
                //window.myPie = new Chart(ctx).Pie(pieData);
                var myLineChart = new Chart(ctx).Line(data);
            }

            $('#tab4').click(function () {
                alert('ura!');
                var ctx = document.getElementById("chart-area").getContext("2d");
                window.myPie = new Chart(ctx).Pie(pieData);
            })

            $('#lst_report_types').change(function () {
                set_settings();
            });

            $('#lst_term_types').change(function () {
                set_settings();
                var term_type_id = $(this).find("option:selected").val();

                if (term_type_id == 2) {
                    $('#<%=ddlDays.ClientID%>').val("0");
                    $('#<%=ddlDays.ClientID%>').attr('disabled', 'disabled');
                    $('#<%=ddlMonths.ClientID%>').removeAttr('disabled');
                }

                if (term_type_id == 3) {
                    $('#<%=ddlDays.ClientID%>').attr('disabled', 'disabled');
                    $('#<%=ddlDays.ClientID%>').val("0");
                    $('#<%=ddlMonths.ClientID%>').attr('disabled', 'disabled');
                    $('#<%=ddlMonths.ClientID%>').val("0");
                }
                if (term_type_id == 1) {
                    $('#<%=ddlDays.ClientID%>').removeAttr('disabled');
                    $('#<%=ddlMonths.ClientID%>').removeAttr('disabled');
                }
            });
        })

        function set_settings() {
            var report_type_id = $('#lst_report_types').find("option:selected").val();
            var term_type_id = $('#lst_term_types').find("option:selected").val();
            $('#<%=txtHidden.ClientID %>').val(report_type_id + "-" + term_type_id);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <p>Поступление налогов по данным из Казначейства</p>
        <table id="tbl_report_settings" style="width: 100%;">
            <tr>
                <td>Тип отчета</td>
                <td>Срок</td>
                <td>Действие</td>
            </tr>
            <tr>
                <td>
                    <select id="lst_report_types" style="width: 130px;">
                        <option value="1">По районам</option>
                        <option value="2">По КБК</option>
                    </select>
                    <br />
                    <div style="visibility: hidden;">
                        <asp:TextBox ID="txtHidden" Width="30px" Text="1-1" runat="server"></asp:TextBox>
                    </div>
                </td>
                <td>
                    <select id="lst_term_types" style="width: 100px;">
                        <option value="1">за день</option>
                        <option value="2">за месяц</option>
                        <option value="3">за год</option>
                    </select>
                    <div>
                        <asp:DropDownList ID="ddlDays" runat="server" Width="55px"></asp:DropDownList>
                        <asp:DropDownList ID="ddlMonths" runat="server" Width="110px"></asp:DropDownList>
                        <asp:DropDownList ID="ddlYears" runat="server" Width="70px"></asp:DropDownList>
                    </div>
                </td>
                <td>
                    <asp:Button ID="dtnGenerate" runat="server" CssClass="btn btn-large btn-success" OnClick="btnGenerate_Click" Text="Генерировать" />

                </td>
            </tr>
        </table>
        <hr />
        <div class="container-fluid">
        </div>
        <div class="container-fluid" style="background-color: #EFEFEF; padding: 5px;">
            <section class="tabs">
                <input id="tab_1" type="radio" name="tab" checked="checked" />
                <input id="tab_2" type="radio" name="tab" />
                <input id="tab_3" type="radio" name="tab" />
                <label for="tab_1" id="tab_l1">Таблица</label>

                <label for="tab_3" id="tab_l3">График</label>
                <div style="clear: both"></div>

                <div class="tabs_cont">
                    <div id="tab_c1" style="height: 600px;">

                        <table class="table table-bordered table-hover" style="width: 530px; float: left;">
                            <thead>
                                <tr>
                                    <th>ДГД</th>
                                    <th>Всего</th>
                                    <th>Мест.</th>
                                    <th>Област.</th>
                                    <th>Респуб.</th>
                                    <th>Спец.транз. счет Нац фонда</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="repReport1" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%#Eval("DGDName") %></td>
                                            <td><%#Eval("TotalValue") %></td>
                                            <td><%#Eval("LocalValue") %></td>
                                            <td><%#Eval("RegionValue") %></td>
                                            <td><%#Eval("RespublicValue") %></td>
                                            <td><%#Eval("SpecValue") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                        <div style="clear:both">
                            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div id="tab_c3">
                        <div id="canvas-holder">
                            <canvas id="chart-area" width="700" height="300" />
                        </div>

                    </div>
                </div>
            </section>

        </div>

    </div>
</asp:Content>
