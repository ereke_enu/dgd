﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGD.Admin
{
    public partial class Materials : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void LoadGridData()
        {

        }

        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridMaterials.PageIndex = e.NewPageIndex;
            LoadGridData();
        }
    }
}