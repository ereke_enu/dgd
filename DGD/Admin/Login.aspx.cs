﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGD.Managers;

namespace DGD.Admin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var user = UserManager.GetUser(txtLogin.Text, txtPassword.Text);
            if (user != null)
            {
                Session[CommonConstants.USER] = user;
                Response.Redirect("/Admin/Default.aspx");
            }
            else
            {
                lblMessage.Text = "Ошибка авторизации";
            }
        }
    }
}