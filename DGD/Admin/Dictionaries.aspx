﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Dictionaries.aspx.cs" Inherits="DGD.Admin.Dictionaries" %>

<%@ Register Src="~/Admin/Controls/CRUDDictionaryControl.ascx" TagName="CRUDDictionaryControl" TagPrefix="control" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <h4>Добавление пунтка списка</h4>
        <control:CRUDDictionaryControl ID="CRUDDictionaryControl1" runat="server" />
    </div>
    <div class="container-fluid" style="margin-top: 20px;">
        <asp:DropDownList ID="ddlTypes" AutoPostBack="true" runat="server" DataTextField="Name" DataValueField="Id" OnSelectedIndexChanged="ddlTypes_IndexChanged"></asp:DropDownList>
    </div>
    <div class="container-fluid">
        <h3>Списки</h3>
        <asp:GridView ID="gridDictionaries" runat="server" AutoGenerateColumns="false"
            CellPadding="4" PageSize="5"
        ForeColor="#333333" GridLines="None" Width="500" AllowPaging="True">
            <Columns>
                <asp:TemplateField HeaderText="#">
                    <ItemTemplate></ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField HeaderText="Тип">
                    <ItemTemplate><%#Eval("TypeName") %></ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField HeaderText="Значение">
                    <ItemTemplate>
                        <%#Eval("Name") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField HeaderText="Код">
                    <ItemTemplate>
                        <%#Eval("Code") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField HeaderText="Тип">
                    <ItemTemplate>
                        <%#Eval("TypeId") %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
