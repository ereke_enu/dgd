﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGD.Managers;

namespace DGD.Admin
{
    public partial class Dictionaries : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var arr = DictionaryManager.GetDictionaries(0);
                ddlTypes.DataSource = DictionaryManager.GetDictionaries(0);
                ddlTypes.DataBind();
               
                
                gridDictionariesDataBind(1);
            }
        }

        private void gridDictionariesDataBind(int typeId)
        {
            gridDictionaries.DataSource = DictionaryManager.GetDictionariesWithTypeName(typeId );
            gridDictionaries.DataBind();
        }

        protected void ddlTypes_IndexChanged(object sender, EventArgs e)
        {
            gridDictionariesDataBind(Convert.ToInt32(ddlTypes.SelectedValue));
        }
    }
}