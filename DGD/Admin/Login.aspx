﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DGD.Admin.Login" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" type="text/css" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 300px; background-color: white; margin: 0 auto;" class="container ">
            <div class="container">
                <h3>Авторизация 
                </h3>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label >Логин</label>
                    <div class="col-sm-10">

                        <asp:TextBox ID="txtLogin" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox">
                                Запомнить меня
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                        <asp:Button ID="btnLogin" runat="server" CssClass="btn btn-default" Text="Войти" OnClick="btnLogin_Click"></asp:Button>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
