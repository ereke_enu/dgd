﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGD.Managers;
using DGD.Models;


namespace DGD.Admin.Controls
{
    public partial class CRUDIncomeControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlDGDs.DataSource = DictionaryManager.GetDictionaries(1);
                ddlDGDs.DataBind();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            
            var arr = txtDate.Text.Split('/');
            string receiptDate = arr[2] + "." + arr[0] + "." + arr[1];
            Report1 report = new Report1();
            report.DGDId = Convert.ToInt32(ddlDGDs.SelectedValue);
            report.ReceiptDate = Convert.ToDateTime(receiptDate);
            report.CreateDate = DateTime.Now;
            var user = (Models.User)Session[CommonConstants.USER];
            report.UserId = user.Id;
            report.TotalValue = Convert.ToDecimal(txtTotalBudget.Text);
            report.LocalValue = Convert.ToDecimal(txtLocalBudget.Text);
            report.RegionValue = Convert.ToDecimal(txtReguionBudget.Text);
            report.RespublicValue = Convert.ToDecimal(txtRespublicBudget.Text);
            report.SpecValue = Convert.ToDecimal(txtSpecBudget.Text);
            if (ReportManager.CreateReport1(report))
            {
                lblMessage.Text = "Запись успешно добавлена";
            }
            else
            {
                lblMessage.Text = "Ошибка";
            }
        }
        
    }
}