﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGD.Managers;
using DGD.Models;

namespace DGD.Admin.Controls
{
    public partial class CRUDMaterialControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlSpecifications.DataSource = DictionaryManager.GetDictionariesWithTypeName(10);
                ddlSpecifications.DataBind();

                ddlLegalStates.DataSource = DictionaryManager.GetDictionariesWithTypeName(12);
                ddlLegalStates.DataBind();

                ddlFinancialSources.DataSource = DictionaryManager.GetDictionariesWithTypeName(14);
                ddlFinancialSources.DataBind();

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                Material material = new Material();
                material.Name = txtName.Text;
                material.SpecificationId = Convert.ToInt32(ddlSpecifications);
                material.LegalStateId = Convert.ToInt32(ddlLegalStates.SelectedValue);
                var arr = txtReceiptDate.Text.Split('/');
                string receiptDate = arr[2] + "." + arr[0] + "." + arr[1];
                material.ReceiptDate = Convert.ToDateTime(receiptDate);
                material.CurrencyId = Convert.ToInt32(rblCurrencies.SelectedValue);
                String savePath = Server.MapPath(@"/Data/materials/");
                String fileName = Guid.NewGuid().ToString();
                savePath += fileName + ".pdf";
                material.FilePath = "";
                material.CreateDate = DateTime.Now;
                material.UserId = ((Models.User)Session[CommonConstants.USER]).Id;
                material.FinancialSourceId = Convert.ToInt32(ddlFinancialSources.SelectedValue);
                FileUpload1.SaveAs(savePath);
                lblMessage.Text = "Your file was saved as " + fileName;
                MaterialManager.CreateMaterial(material);
            }
            else
            {
                lblMessage.Text = "You did not specify a file to upload.";
            }
        }
    }
}