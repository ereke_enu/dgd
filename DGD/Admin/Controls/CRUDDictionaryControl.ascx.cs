﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGD.Managers;
using DGD.Models;

namespace DGD.Admin.Controls
{
    public partial class CRUDDictionaryControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlTypesDataBind();
            }
        }

        private void ddlTypesDataBind()
        {
            ddlTypes.Items.Clear();
            ddlTypes.DataSource = DictionaryManager.GetDictionaries(0);
            ddlTypes.DataBind();
            ddlTypes.Items.Add(new ListItem() { Text = "Корневой элемент", Value = "0" });
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            BaseDictionary dict = new BaseDictionary();
            dict.Name = txtName.Text;
            dict.TypeId = Convert.ToInt32(ddlTypes.SelectedValue);
            dict.Code = Convert.ToInt32(txtCode.Text);
            if (DictionaryManager.CreateDictionary(dict))
            {
                lblMessage.Text = "Запись успешно добавлена";
                ddlTypesDataBind();
            }
            else
            {
                lblMessage.Text = "Ошибка";
            }
        }
    }
}