﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRUDDictionaryControl.ascx.cs" Inherits="DGD.Admin.Controls.CRUDDictionaryControl" %>
<table>
    <tr>
        <td>Тип

        </td>
        <td>
            <asp:DropDownList ID="ddlTypes" DataTextField="Name" DataValueField="Id" runat="server"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>Значение</td>
        <td>
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
    </tr>
    <tr>
        <td>Код</td>
        <td>
            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2" >
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <asp:Button ID="btnSave" style="float:right;" runat="server" OnClick="btnSave_Click" Text="Сохранить" />
        </td>
    </tr>
</table>
