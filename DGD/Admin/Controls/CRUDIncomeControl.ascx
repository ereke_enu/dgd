﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRUDIncomeControl.ascx.cs" Inherits="DGD.Admin.Controls.CRUDIncomeControl" %>
<div class="container-fluid">
    <h4>Отчет по районам
    </h4>
    <table>
        <tr>
            <td>ДГД</td>
            <td>
                <asp:DropDownList ID="ddlDGDs" runat="server" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Дата</td>
            <td>
                <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Местный бюджет</td>
            <td><asp:TextBox ID="txtLocalBudget" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Областной бюджет</td>
            <td>
                <asp:TextBox ID="txtReguionBudget" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Респуб. бюджет</td>
            <td>
                <asp:TextBox ID="txtRespublicBudget" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Спец.транз счет Нац фонда</td>
            <td>
                <asp:TextBox ID="txtSpecBudget" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Всего</td>
            <td>
                <asp:TextBox ID="txtTotalBudget" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                  <asp:Button CssClass="btn" style="float:right" ID="btnSave" runat="server" Text="Сохранить" OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
    <div>
       
        <script>
            $("#<%=txtDate.ClientID%>").datepicker();
        </script>
    </div>
</div>

