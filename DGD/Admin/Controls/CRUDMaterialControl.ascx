﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRUDMaterialControl.ascx.cs" Inherits="DGD.Admin.Controls.CRUDMaterialControl" %>

    <table>
        <tr>
            <td>Наименование</td>
            <td>
                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Спецификация</td>
            <td>
                <asp:DropDownList ID="ddlSpecifications" runat="server" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Гос.Учреждение</td>
            <td>
                <asp:DropDownList ID="ddlLegalStates" runat="server" DataTextField="Name" DataValueField="Id"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Дата </td>
            <td>
                <asp:TextBox ID="txtReceiptDate" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Валюта</td>
            <td>
                <asp:RadioButtonList ID="rblCurrencies" runat="server" RepeatDirection="Horizontal" Width="200px">
                    <asp:ListItem Text="KZT" Value="0"></asp:ListItem>
                    <asp:ListItem Text="USD" Value="1"></asp:ListItem>
                </asp:RadioButtonList></td>
        </tr>
        <tr>
            <td>Путь к файлу</td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Источник финансирования</td>
            <td>
                <asp:DropDownList ID="ddlFinancialSources" DataTextField="Name" DataValueField="Id" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Сохранить" />
            </td>
        </tr>
    </table>

<script>
    $("#<%=txtReceiptDate.ClientID%>").datepicker();
</script>
