﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Materials.aspx.cs" Inherits="DGD.Admin.Materials" %>

<%@ Register Src="~/Admin/Controls/CRUDMaterialControl.ascx" TagName="CRUDMaterialControl" TagPrefix="control" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/Styles/jquery-ui.css" />
    <script type="text/javascript" src="/Scripts/jquery-ui.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <h4>Добавление материала</h4>
        <control:CRUDMaterialControl ID="CRUDMaterialControl1" runat="server" />
    </div>
    <div class="container-fluid">
        <asp:GridView ID="gridMaterials" runat="server" AutoGenerateColumns="False" CellPadding="4" PageSize="5"
            ForeColor="#333333" GridLines="None" Width="500" AllowPaging="True"
            OnPageIndexChanging="grdData_PageIndexChanging">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="Name"></asp:BoundField>
            </Columns>
            <EditRowStyle BackColor="#999999"></EditRowStyle>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True"
                ForeColor="White"></FooterStyle>
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True"
                ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#284775" ForeColor="White"
                HorizontalAlign="Center"></PagerStyle>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333"></RowStyle>
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True"
                ForeColor="#333333"></SelectedRowStyle>
            <SortedAscendingCellStyle BackColor="#E9E7E2"></SortedAscendingCellStyle>
            <SortedAscendingHeaderStyle BackColor="#506C8C"></SortedAscendingHeaderStyle>
            <SortedDescendingCellStyle BackColor="#FFFDF8"></SortedDescendingCellStyle>
            <SortedDescendingHeaderStyle BackColor="#6F8DAE"></SortedDescendingHeaderStyle>
        </asp:GridView>
    </div>
</asp:Content>
