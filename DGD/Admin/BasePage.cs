﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DGD.Managers;
using DGD.Models;

namespace DGD.Admin
{
    public class BasePage : System.Web.UI.Page
    {
        public User USER { get { return (Models.User)Session[CommonConstants.USER]; } }

        public BasePage()
        {
            //if (Session[CommonConstants.USER] != null) USER = (Models.User)Session[CommonConstants.USER];
        }

        public void RedirectIfNotAuthorized()
        {
            
        }

        protected override void InitializeCulture()
        {
            base.InitializeCulture();
            if (Session[CommonConstants.USER] == null) Response.Redirect("/Admin/Login.aspx");
        }
    }
}