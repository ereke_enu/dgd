﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Incomes.aspx.cs" Inherits="DGD.Admin.Incomes" %>

<%@ Register Src="~/Admin/Controls/CRUDIncomeControl.ascx" TagName="CRUDIncomeControl" TagPrefix="control" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/Styles/jquery-ui.css" />
    <script type="text/javascript" src="/Scripts/jquery-ui.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <control:CRUDIncomeControl ID="CRUDIncomeControl1" runat="server"></control:CRUDIncomeControl>
    
</asp:Content>
