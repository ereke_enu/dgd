﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Materials.aspx.cs" Inherits="DGD.Materials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Регион</th>
                    <th>Списификация</th>
                    <th>Гос.учрежд</th>
                    <th>Источн.фин</th>
                    <th>Дата</th>
                    <th>Валюта</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="repMaterials" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td><a href="Data/materials/27.07.15.pdf"><%#Eval("Name") %></a></td>
                            <td>4301-Мангистауская обл</td>
                            <td><%#Eval("Name") %></td>
                            <td><%#Eval("SpecificationName") %></td>
                            <td><%#Eval("Name") %></td>
                            <td><%#Eval("Name") %></td>
                            <td><%#Eval("Name") %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
